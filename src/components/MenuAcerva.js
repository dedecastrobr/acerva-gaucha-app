import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { loadCard, loadProfile, logOut } from '../actions';
import {
  Card,
  CardSection,
  Button,
  Spinner
} from './common';

class MenuAcerva extends Component {

  renderSpinner(){
    if(this.props.loading) {
      return <Spinner size='small' />;
    }
  }

  render(){
    Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.PORTRAIT);
    return(
      <Card>
        <CardSection>
          <Button onPress={this.props.loadProfile} >
            Meus Dados
          </Button>
        </CardSection>
        <CardSection>
          <Button onPress={this.props.loadCard} >
            Carteira Digital
          </Button>
        </CardSection>
        <CardSection>
          <Button onPress={this.props.logOut} >
            Sair
          </Button>
        </CardSection>
        <CardSection>
          {this.renderSpinner()}
        </CardSection>
      </Card>


    );
  }
}

const mapStateToProps = state => {
  return{
    loading: state.auth.loading
  };
};
export default connect(mapStateToProps, { loadCard, loadProfile, logOut })(MenuAcerva);
