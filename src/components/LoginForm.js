import React, { Component } from 'react';
import { Text, Image, View } from 'react-native';
import { connect } from 'react-redux';
import {
  usernameChanged,
  passwordChanged,
  loginUser,
  logOut,
  checkLogin
} from '../actions';

import {
  Card,
  CardSection,
  Button,
  Input,
  Spinner
} from './common';


class LoginForm extends Component {

  componentWillMount(){
    this.props.checkLogin();
  }

  proceedLogin(){
    const { username, password } = this.props;
    this.props.loginUser({ username, password });
  }

  onUsernameChange(text){
    this.props.usernameChanged(text);
  }

  onPasswordChange(text){
    this.props.passwordChanged(text);
  }

  renderButton(){
    if(this.props.loading) {
      return <Spinner size='small' />;
    }
    return(
      <Button onPress={this.proceedLogin.bind(this)} >
        Entrar
      </Button>
    );
  }

  render() {
    return (
      <Card>
        <View style={{ alignItems: 'center', backgroundColor: '#fff' }}>
          <Image
            source={require('../../assets/img/logo_300p.jpg')} />
        </View>
        <CardSection>
          <Input
            placeHolder="o mesmo do site"
            label="Usuário"
            value={this.props.username}
            onChangeText={this.onUsernameChange.bind(this)}
          />
        </CardSection>
        <CardSection>
          <Input
            secureTextEntry
            placeHolder="senha"
            label="Senha"
            value={this.props.password}
            onChangeText={this.onPasswordChange.bind(this)}
          />
        </CardSection>

        <Text style={styles.errorTextStyle}>
          {this.props.error}
        </Text>

        <CardSection>

          {this.renderButton()}

        </CardSection>
      </Card>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'red'
  }
}

const mapStateToProps = state => {
  return{
    username: state.auth.username,
    password: state.auth.password,
    error: state.auth.error,
    loading: state.auth.loading
  };
};

export default connect(mapStateToProps, {
                                          usernameChanged,
                                          passwordChanged,
                                          loginUser,
                                          logOut,
                                          checkLogin
                                        } )(LoginForm);
