import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { showCard, showProfile, logOut } from '../actions';
import { Card, CardSection, Button } from './common';

class ProfileAcerva extends Component {
  render(){
    return(
      <Card>
        <Card>
          <CardSection>
            <Text style={styles.labelTextStyle} > Nome: </Text>
          </CardSection>
          <CardSection>
            <Text style={styles.dataTextStyle} > {this.props.name} </Text>
          </CardSection>
        </Card>
        <Card>
          <CardSection>
            <Text style={styles.labelTextStyle} > Email: </Text>
          </CardSection>
          <CardSection>
            <Text style={styles.dataTextStyle} > {this.props.email} </Text>
          </CardSection>
        </Card>
        <Card>
          <CardSection>
            <Text style={styles.labelTextStyle} > Username: </Text>
          </CardSection>
          <CardSection>
            <Text style={styles.dataTextStyle} > {this.props.username} </Text>
          </CardSection>
        </Card>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  labelTextStyle: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  dataTextStyle: {
    fontSize: 22,
    alignSelf: 'flex-end'
  }
});


const mapStateToProps = state => {
  return{
    name: state.auth.name,
    email: state.auth.email,
    username: state.auth.username
  };
};
export default connect(mapStateToProps, { logOut })(ProfileAcerva);
