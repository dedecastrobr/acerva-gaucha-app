import React, { Component } from 'react';
import { Text, StyleSheet, View } from 'react-native';

class Header extends Component {

  render() {
    const styles = StyleSheet.create({
      textStyle: {
        fontSize: 20
      },
      viewStyle: {
        backgroundColor: '#54c403',
        alignItems: 'center',
        justifyContent: 'center',
        height: 60,
        paddingTop: 15,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
      }
    });

    const { textStyle, viewStyle } = styles;

    return (
      <View style={viewStyle}>
        <Text style={textStyle}>{this.props.headerText}</Text>
      </View>
    );
  }
}

export { Header }
