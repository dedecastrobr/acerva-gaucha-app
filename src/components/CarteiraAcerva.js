import React, { Component } from 'react';
import { Text, Image, View, StyleSheet} from 'react-native';
import { Card, CardSection } from './common';
import { connect } from 'react-redux';
import moment from 'moment';


class CarteiraAcerva extends Component {

  componentWillMount() {
    Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.LANDSCAPE);
  }

  render(){
    return(
      <View style={styles.containerStyle} >
          <View style={{flex: 0.6}}>
            <View style={styles.containerStyle, {flex: 0.4, justifyContent: 'space-around'}} >
              <Text style={styles.label}>Nome: </Text>
              <Text style={styles.data}>{this.props.name}</Text>
            </View>
            <View style={styles.containerStyle, {flex: 0.4, flexDirection: 'row', justifyContent: 'space-around'}} >
              <View>
                  <Text style={styles.label}>Status: </Text>
                  <Text style={styles.data}>{this.props.status}</Text>
              </View>
              <View>
                  <Text style={styles.label}>Válido Até: </Text>
                  <Text style={styles.data}>{this.props.expiration_date}</Text>
              </View>
            </View>
            <View style={styles.containerStyle, {flex: 0.3, alignItems: 'flex-end'}} >
              <Text style={styles.label}>Matrícula n°: </Text>
              <Text style={styles.data}>{this.props.matricula}</Text>
            </View>
          </View>
          <View style={{flex: 0.4}}>
              <Image
                source={require('../../assets/img/logo_300p.jpg')}
                style={styles.containerStyle, styles.image} />
          </View>
      </View>

    );
  }
}

const styles = StyleSheet.create({
                          image: {
                            flex: 1,
                            justifyContent: 'center',
                            width: 280,
                            height: 250  },
                          containerStyle: {
                            flex: 1,
                            flexDirection: 'row',
                            backgroundColor: 'white',
                            justifyContent: 'space-around',
                            borderWidth: 1,
                            borderRadius: 2,
                            borderColor: '#ddd',
                            borderBottomWidth: 0,
                            shadowColor: '#000',
                            shadowOffset: { width: 0, height: 2 },
                            shadowOpacity: 0.1,
                            shadowRadius: 2,
                            elevation: 1,
                            marginLeft: 5,
                            marginRight: 5,
                            marginTop: 10
                          },
                          data: {
                            fontFamily: 'Raleway-ExtraBold',
                            fontSize: 24
                          },
                          label: {
                            fontFamily: 'Raleway-ExtraLight',
                            fontSize: 18
                          }
                        });


const mapStateToProps = state => {

  return{
    name: state.auth.name,
    status: state.auth.status,
    expiration_date: moment(state.auth.expiration_date).format("DD/MM/YYYY"),
    matricula: state.auth.matricula,
    loading: state.auth.loading
  };
};

export default connect(mapStateToProps, {} )(CarteiraAcerva)
