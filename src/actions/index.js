import { Actions } from 'react-native-router-flux';
import {
  USERNAME_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER,
  LOG_OUT,
  LOGOUT_USER_SUCCESS,
  CHECK_LOGIN,
  SHOW_PROFILE,
  LOAD_PROFILE,
  LOAD_CARD,
  SHOW_CARD
} from './types';

const URL_DOMAIN = "http://www.acervagaucha.com.br";
const URL_CARD = URL_DOMAIN + "/index.php?option=com_jbackend&view=request&action=get&module=user&resource=carteirinha";
const URL_LOGOUT = URL_DOMAIN + "/index.php?option=com_jbackend&view=request&action=get&module=user&resource=logout";
const URL_PROFILE = URL_DOMAIN + "/index.php?option=com_jbackend&view=request&action=get&module=user&resource=profile";

export const usernameChanged = (text) => {
  return {
    type: USERNAME_CHANGED,
    payload: text
  };
}

export const passwordChanged = (text) => {
  return {
    type: PASSWORD_CHANGED,
    payload: text
  };
}

export const loginUser = ({ username, password }) => {

  const URL_LOGIN = URL_DOMAIN + "/index.php?option=com_jbackend&view=request&action=post&module=user&resource=login&username=" + username + "&password=" + password;

  return (dispatch) => {
    dispatch({ type: LOGIN_USER  });
    fetch(URL_LOGIN, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    })
    .then((response) => response.json())
    .then((res) => {
      console.log(res);
      return (res.status == 'ok' || res.error_description == 'Already logged in') ? dispatchAction(dispatch, LOGIN_USER_SUCCESS) : dispatchAction(dispatch, LOGIN_USER_FAIL)
    })
    .catch( (error) => {
      console.error("Erro de login!");
      console.error(error);
    });
  }
}


export const loadCard = () => {
  return (dispatch) => {
    dispatch({ type: LOAD_CARD  });
    fetch(URL_CARD, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    })
    .then((response) => response.json())
    .then((res) => {
      console.log(res);
      return (res.status == 'ok') ? dispatchAction(dispatch, SHOW_CARD, res) : dispatchAction(dispatch, LOGIN_USER_FAIL)
    })
    .catch( (error) => {
      console.error("Erro ao buscar carteira!");
      console.error(error);
    });
  }
}


export const loadProfile = () => {
  return (dispatch) => {
    dispatch({ type: LOAD_PROFILE });
    fetch(URL_PROFILE, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    })
    .then((response) => response.json())
    .then((res) => { return res.status == 'ok' ? dispatchAction(dispatch, SHOW_PROFILE, res) : dispatchAction(dispatch, SHOW_PROFILE_FAIL) })
    .catch( (error) => {
      console.error("Erro ao exibir Profile!");
      console.error(error);
    });
  }
}

export const logOut = () => {
  return (dispatch) => {
    dispatch({ type: LOG_OUT  });
    fetch(URL_LOGOUT, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    })
    .then((response) => response.json())
    .then((res) => { return res.status == 'ok' ? dispatchAction(dispatch, LOGOUT_USER_SUCCESS) : dispatchAction(dispatch, LOGOUT_USER_FAIL) })
    .catch( (error) => {
      console.error("Erro de logOut!");
      console.error(error);
    });
  }
}

export const checkLogin = () => {
  return (dispatch) => {
    fetch(URL_PROFILE, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    })
    .then((response) => response.json())
    .then((res) => { return res.status == 'ok' ? dispatchAction(dispatch, LOGIN_USER_SUCCESS) : dispatch({ type: CHECK_LOGIN  }); } )
    .catch( (error) => {
      console.error("Erro verificando sessão!");
      console.error(error);
    });
  }
}

const dispatchAction = (dispatch, action, payload=null) => {
  switch (action) {
    case LOGIN_USER_SUCCESS:
      dispatch({ type: LOGIN_USER_SUCCESS });
      Actions.main();
      break;
    case LOGIN_USER_FAIL:
      dispatch({ type: LOGIN_USER_FAIL });
      Actions.auth();
      break;
    case LOGOUT_USER_SUCCESS:
      dispatch({ type: LOGOUT_USER_SUCCESS });
      Actions.auth();
      break;
    case SHOW_PROFILE:
      dispatch({ type: SHOW_PROFILE, payload: payload });
      Actions.profileAcerva();
      break;
    case LOAD_PROFILE:
      dispatch({ type: LOAD_PROFILE });
      break;
    case SHOW_CARD:
      dispatch({ type: SHOW_CARD, payload: payload });
      Actions.carteiraAcerva();
      break;

    default:
      console.warn("Unhandled action on dispatchAction: " + action);
  }
}
