import React from 'react';
import { Router, Scene } from 'react-native-router-flux';
import  LoginForm  from './components/LoginForm';
import MenuAcerva from './components/MenuAcerva';
import CarteiraAcerva from './components/CarteiraAcerva';
import ProfileAcerva from './components/ProfileAcerva';

const RouterComponent = () =>  {
  return (
    <Router
      sceneStyle={styles.sceneStyle}
      navigationBarStyle={styles.navBar}
      titleStyle={styles.navBarTitle} >
      <Scene key="root" hideNavBar>
        <Scene key="auth">
          <Scene key="login" component={LoginForm} title="Login" initial />
        </Scene>
        <Scene key="main">
          <Scene key="menuAcerva" component={MenuAcerva} title="Menu do Associado" />
          <Scene key="carteiraAcerva" component={CarteiraAcerva} title="Associação de Cervejeiros Artesanais do Rio Grande do Sul"/>
          <Scene key="profileAcerva" component={ProfileAcerva} title="Meus Dados" />
        </Scene>
      </Scene>
    </Router>
  )
}

const styles = {
  sceneStyle: {
    paddingTop: 15
  },
  navBar: {
    backgroundColor: '#01a54f',
    height: 70
  },
  navBarTitle: {
    color: '#FFFFFF',
    fontSize: 20,
    alignSelf: 'center'
  }
};

export default RouterComponent;
