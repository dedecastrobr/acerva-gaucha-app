import {
  USERNAME_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER,
  CHECK_LOGIN,
  LOGOUT_USER_SUCCESS,
  SHOW_PROFILE,
  LOAD_PROFILE,
  LOAD_CARD,
  SHOW_CARD
} from '../actions/types';

const INITIAL_STATE = {
  loggedIn: false,
  username: '',
  password: '',
  error: '',
  loading: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case USERNAME_CHANGED:
      return { ...state, username: action.payload };
      break;
    case PASSWORD_CHANGED:
      return { ...state, password: action.payload };
      break;
    case LOGIN_USER_SUCCESS:
      return { ...state, ...INITIAL_STATE, loggedIn: true }
      break;
    case LOGOUT_USER_SUCCESS:
      return { ...state, ...INITIAL_STATE, loggedIn: false, error:'' }
      break;
    case LOGIN_USER_FAIL:
      return { ...state, error: 'Falha de login.', loading: false }
      break;
    case LOGIN_USER:
      return { ...state, error: '', loading: true }
      break;
    case CHECK_LOGIN:
      return { ...state, error: '' }
      break;
    case SHOW_PROFILE:
      return {
              ...state,
              error: '',
              loading: false,
              name: action.payload.name,
              email: action.payload.email,
              username: action.payload.username
            }
      break;
    case LOAD_PROFILE:
      return { ...state, error: '', loading: true }
      break;
    case LOAD_CARD:
      return { ...state, error: '', loading: true }
      break;
    case SHOW_CARD:
      return {
              ...state,
              error: '',
              loading: false,
              name: action.payload.name,
              status: action.payload.status_associacao,
              expiration_date: action.payload.data_expiracao.date,
              matricula: "99999"
            }
      break;
    default:
      return state;
  }
}
