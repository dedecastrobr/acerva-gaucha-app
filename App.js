import React, { Component } from 'react';
import { StyleSheet, View , Text, Button, ToastAndroid } from 'react-native';
import { Header } from './src/components/common'
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './src/reducers';
import Router from './src/Router';
import { Font } from 'expo';

const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

export default class App extends Component {
  render() {
    Font.loadAsync({
        'Raleway-ExtraBold': require('./assets/fonts/Raleway-ExtraBold.ttf'),
        'Raleway-ExtraLight': require('./assets/fonts/Raleway-ExtraLight.ttf')
    });
    return (
      <Provider store={store}>
        <Router />
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
